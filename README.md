<div align="right">
    <img src="https://img.shields.io/badge/HTML5-E44D26?style=for-the-badge&logo=html5&logoColor=white" /> <br>
    <img src="https://img.shields.io/badge/CSS3-2965f1?style=for-the-badge&logo=css3&logoColor=white" />
</div>


<div align="center">
<h1>HTML od Podstaw: Moja Podróż przez Świat Tworzenia Stron Internetowych</h1>
<a href="https://www.udemy.com/course/kurs-html-dla-poczatkujacych/">
<img src="https://img-c.udemycdn.com/course/750x422/5939470_f38f_2.jpg" width="500" border="10" >
</a>
</div>  

## Wprowadzenie

Witam Chciałbym podzielić się moją podróżą przez kurs "HTML od Podstaw", który niedawno ukończyłem. Ten kurs okazał się
być kluczem do otwarcia dla mnie drzwi do fascynującego świata web development.

## Spis treści

<details >
    <summary><a href="01.Podstawowa%20struktura%20dokumentu%20HTML">01.Podstawowa struktura dokumentu HTML</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;p&gt;&lt;/p&gt;</dd>
        <dd>&lt;h1&gt;&lt;/h1&gt;</dd>
        <dd>&lt;h2&gt;&lt;/h2&gt;</dd>
        <dd>&lt;h3&gt;&lt;/h3&gt;</dd>
        <dd>&lt;h4&gt;&lt;/h4&gt;</dd>
        <dd>&lt;h5&gt;&lt;/h5&gt;</dd>
        <dd>&lt;h6&gt;&lt;/h6&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="02.Znaczniki%20HTML%20do%20pracy%20z%20tekstem">02.Znaczniki HTML do pracy z tekstem</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;em&gt;&lt;/em&gt;</dd>
        <dd>&lt;u&gt;&lt;/u&gt;</dd>
        <dd>&lt;hr&gt;</dd>
        <dd>&lt;i&gt;&lt;/i&gt;</dd>
        <dd>&lt;strong&gt;&lt;/strong&gt;</dd>
        <dd>&lt;b&gt;&lt;/b&gt;</dd>
        <dd>&lt;big&gt;&lt;/big&gt;</dd>
        <dd>&lt;small&gt;&lt;/small&gt;</dd>
        <dd>&lt;sup&gt;&lt;/sup&gt;</dd>
        <dd>&lt;sub&gt;&lt;/sub&gt;</dd>
        <dd>&lt;ins&gt;&lt;/ins&gt;</dd>
        <dd>&lt;mark&gt;&lt;/mark&gt;</dd>
        <dt>STRONY</dt></dt>
        <dd><a href="https://htmlreference.io/">HTML reference</a></dd>
    </dl>
</details>
<details>
    <summary><a href="03.Nag%C5%82%C3%B3wki">03.Nagłówki</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;p&gt;&lt;/p&gt;</dd>
        <dd>&lt;h1&gt;&lt;/h1&gt;</dd>
        <dd>&lt;h2&gt;&lt;/h2&gt;</dd>
        <dd>&lt;h3&gt;&lt;/h3&gt;</dd>
        <dd>&lt;h4&gt;&lt;/h4&gt;</dd>
        <dd>&lt;h5&gt;&lt;/h5&gt;</dd>
        <dd>&lt;h6&gt;&lt;/h6&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="04.Linki%20%28hiper%C5%82%C4%85cza%29">04.Linki (hiperłącza)</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;a&gt;&lt;/a&gt;</dd>
        <dd>&lt;style&gt;&lt;/style&gt;</dd>
        <dd>&lt;br&gt;</dd>
        <dt>CSS</dt>
        <dd>.classname</dd>
        <dd>#idname</dd>
        <dd>width</dd>
</details>
<details>
    <summary><a href="05.Audio">05.Audio</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;audio&gt;&lt;/audio&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="06.Wideo">06.Wideo</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;video&gt;&lt;/video&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="07.Listy">07.Listy</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;ul&gt;&lt;/ul&gt;</dd>
        <dd>&lt;ol&gt;&lt;/ol&gt;</dd>
        <dd>&lt;dl&gt;&lt;/dl&gt;</dd>
        <dd>&lt;dt&gt;&lt;/dt&gt;</dd>
        <dd>&lt;dd&gt;&lt;/dd&gt;</dd>
        <dd>&lt;li&gt;&lt;/li&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="08.Tabele">08.Tabele</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;table&gt;&lt;/table&gt;</dd>
        <dd>&lt;tr&gt;&lt;/tr&gt;</dd>
        <dd>&lt;th&gt;&lt;/th&gt;</dd>
        <dd>&lt;td&gt;&lt;/td&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="09.CSS%20do%20pliku%20HTML">09.CSS do pliku HTML</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;h1&gt;&lt;/h1&gt;</dd>
        <dd>&lt;img&gt;</dd>
        <dd>&lt;link&gt;</dd>
        <dt>CSS</dt>
        <dd>color</dd>
        <dd>font-style</dd>
        <dd>font-size</dd>
        <dd>height</dd>
        <dd>border-radius</dd>
    </dl>
</details>
<details>
    <summary><a href="10.DOM%20-%20Obiektowy%20Model%20Dokumentu">10.DOM - Obiektowy Model Dokumentu</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;link&gt;</dd>
        <dd>&lt;h1&gt;&lt;/h1&gt;</dd>
        <dd>&lt;img&gt;</dd>
        <dt>CSS</dt>
        <dd>color</dd>
        <dd>font-style</dd>
        <dd>font-size</dd>
        <dd>height</dd>
        <dd>border-radius</dd>
        <dd>border</dd>
        <dd>margin</dd>
    </dl>
</details>
<details>
    <summary><a href="11.SPAN%20i%20DIV">11.SPAN i DIV</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;div&gt;&lt;/div&gt;</dd>
        <dd>&lt;h2&gt;&lt;/h2&gt;</dd>
        <dd>&lt;p&gt;&lt;/p&gt;</dd>
        <dd>&lt;header&gt;&lt;/header&gt;</dd>
        <dd>&lt;ul&gt;&lt;/ul&gt;</dd>
        <dd>&lt;li&gt;&lt;/li&gt;</dd>
        <dd>&lt;main&gt;&lt;/main&gt;</dd>
        <dt>CSS</dt>
        <dd>font-weight</dd>
        <dd>width</dd>
        <dd>list-style</dd>
        <dd>display</dd>
    </dl>
</details>
<details>
    <summary><a href="12.Sekcja%20HEAD">12.Sekcja HEAD</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;meta name="description"&gt;</dd>
        <dd>&lt;meta name="keywords"&gt;</dd>
        <dd>&lt;meta author="Jacek Podgórni"&gt;</dd>
    </dl>
</details>
<details>
    <summary><a href="13.Formularze">13.Formularze</a></summary>
    <dl style="margin-left: 30px">
        <dt>HTML</dt>
        <dd>&lt;form&gt;&lt;/form&gt;</dd>
        <dd>&lt;label&gt;&lt;/label&gt;</dd>
        <dd>&lt;button&gt;&lt;/button&gt;</dd>
        <dd>&lt;fieldset&gt;&lt;/fieldset&gt;</dd>
        <dd>&lt;legend&gt;&lt;/legend&gt;</dd>
        <dd>&lt;input type="number" name="" id=""&gt;</dd>
        <dd>&lt;input type="checkbox" name="text" id="&gt;</dd>
        <dd>&lt;input type="date" name="" id=""&gt;</dd>
    </dl>
</details>

## Co Mnie Nauczył Kurs?

Podczas kursu "HTML od Podstaw" zdobyłem solidne podstawy w zakresie HTML, które są niezbędne do tworzenia prostych, ale
funkcjonalnych stron internetowych. Oto kilka kluczowych umiejętności, które nabyłem:

- **Podstawy HTML**: Zrozumienie, czym jest HTML i jak jest używane do strukturowania treści na stronach internetowych.
- **Struktura Strony**: Nauka tworzenia zrozumiałych i dobrze zorganizowanych stron za pomocą odpowiednich tagów i
  atrybutów HTML.
- **Tworzenie Pierwszej Strony**: Praktyczne doświadczenie w tworzeniu mojej pierwszej strony internetowej, od koncepcji
  po wdrożenie.
- **Stylizacja za pomocą CSS**: Podstawy stylizacji elementów HTML za pomocą CSS, co pozwoliło mi na tworzenie bardziej
  atrakcyjnych wizualnie stron.
- **Multimedia i Formularze**: Umiejętność wstawiania obrazów, wideo i tworzenia formularzy, co znacznie rozszerza
  możliwości interakcji z użytkownikami.

## Dlaczego Wybrałem Ten Kurs?

Wybór kursu "HTML od Podstaw" był dla mnie oczywisty ze względu na jego bezpośrednie podejście do nauki. Kurs jest
bezpłatny, co było dużym plusem, biorąc pod uwagę, że jestem na początku swojej drogi w web development.

## Moje Doświadczenie

Przechodzenie przez materiały kursu było satysfakcjonującym doświadczeniem. Każdy moduł był dobrze przemyślany i
stopniowo wprowadzał nowe koncepcje, co ułatwiło mi zrozumienie i zapamiętanie informacji.

## Podsumowanie

Ukończenie kursu "HTML od Podstaw" było dla mnie kamieniem milowym w mojej podróży do web development. Dzięki temu
kursowi nie tylko zdobyłem podstawowe umiejętności w HTML, ale także zainspirowałem się do dalszej nauki i rozwoju w tej
dziedzinie. Serdecznie zachęcam każdego, kto jest zainteresowany tworzeniem stron internetowych, do spróbowania sił w
tym kursie. Naprawdę warto!

<div>
    <a href="mailto:jackoski@gmail.com" >
    <img src="https://img.shields.io/badge/Jacek-Podg%C3%B3rni-teal">
    </a>
</div>

![HTML5](https://img.shields.io/badge/HTML%205-grey?style=for-the-badge&logo=html5)  
![CSS3](https://img.shields.io/badge/CSS%203-grey?style=for-the-badge&logo=css3&logoColor=blue)  
![GitLab Badge](https://img.shields.io/badge/GitLab-FC6D26?logo=gitlab&logoColor=fff&style=plastic)  
